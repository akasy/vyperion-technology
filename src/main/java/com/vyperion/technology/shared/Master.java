package com.vyperion.technology.shared;

import org.springframework.http.MediaType;

public class Master {

    public final static String media = MediaType.APPLICATION_JSON_UTF8_VALUE;
}
