package com.vyperion.technology.configuration;

import com.vyperion.technology.entities.subtype.SubType;
import com.vyperion.technology.entities.technology.Technology;
import com.vyperion.technology.entities.type.Type;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class IdConfiguration implements RepositoryRestConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Technology.class);
        config.exposeIdsFor(Type.class);
        config.exposeIdsFor(SubType.class);
    }
}
