package com.vyperion.technology.entities.technology;

import com.vyperion.technology.shared.Master;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/technology")
public class TechnologyController {

    private final TechnologyService technologyService;

    public TechnologyController(TechnologyService technologyService) {
        this.technologyService = technologyService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = Master.media)
    public List<Technology> getAllTechnologies() {
        return technologyService.getAllTechnologies();
    }

    //https://stackoverflow.com/questions/13715811/requestparam-vs-pathvariable
//    @GetMapping(value = "/{id}", produces="application/json")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = Master.media)
    public Technology getTechnologyById(@PathVariable String id) {
        return technologyService.getTechnologyById(id);
    }
}
