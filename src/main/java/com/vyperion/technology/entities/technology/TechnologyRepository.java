package com.vyperion.technology.entities.technology;

import org.springframework.data.jpa.repository.JpaRepository;

//@RepositoryRestResource(collectionResourceRel = "technology", path = "technology")
public interface TechnologyRepository extends JpaRepository<Technology, String> {

}
