package com.vyperion.technology.entities.technology;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "technology")
public class Technology {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    @NotNull
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "website")
    private String website;

    @Column(name = "description")
    private String description;

    @Column(name = "image")
    private String image;

    @Column(name = "subtype")
    private int subtype;

//    @JoinColumn(name="subtype")
//    @ManyToOne
//    private SubType subtype;


}

