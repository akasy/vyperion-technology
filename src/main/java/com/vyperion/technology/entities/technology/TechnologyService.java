package com.vyperion.technology.entities.technology;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TechnologyService {

    private TechnologyRepository technologyRepository;

    public TechnologyService(TechnologyRepository technologyRepository) {
        this.technologyRepository = technologyRepository;
    }

    public List<Technology> getAllTechnologies() {
        return technologyRepository.findAll();
    }


    public Technology getTechnologyById(String id) {
        return technologyRepository.findById(id).orElse(new Technology());
    }
}
