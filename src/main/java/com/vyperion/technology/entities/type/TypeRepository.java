package com.vyperion.technology.entities.type;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

//@RepositoryRestResource(collectionResourceRel = "type", path = "type")
public interface TypeRepository extends JpaRepository<Type, Integer> {

    Optional<Type> findByName(String name);

}
