package com.vyperion.technology.entities.type;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeService {

    private TypeRepository typeRepository;

    public TypeService(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

     List<Type> getAllTypes() {
        return typeRepository.findAll();
    }

    Type getTypeById(int id) {
        return typeRepository.findById(id).orElse(new Type());
    }
}
