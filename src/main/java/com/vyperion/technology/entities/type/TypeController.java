package com.vyperion.technology.entities.type;


import com.vyperion.technology.shared.Master;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/type")
public class TypeController {

    private final TypeService typeService;

    public TypeController(TypeService typeService) {
        this.typeService = typeService;
    }

//    @GetMapping(value = "", produces="application/json")
    @RequestMapping(value = "", method = RequestMethod.GET, produces = Master.media)
    public List<Type> getAllTypes() {
        return typeService.getAllTypes();
    }

//    @GetMapping(value = "/{id}", produces="application/json")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = Master.media)
    public Type getTypeById(@PathVariable int id) {
        return typeService.getTypeById(id);
    }
}
