package com.vyperion.technology.entities.subtype;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubTypeService {

    private final SubTypeRepository subtypeRepository;

    public SubTypeService(SubTypeRepository subtypeRepository) {
        this.subtypeRepository = subtypeRepository;
    }

    List<SubType> getAllSubTypes() {
        return subtypeRepository.findAll();
    }


    SubType getSubTypeById(int id) {
        return subtypeRepository.findById(id).orElse(new SubType());
    }

}
