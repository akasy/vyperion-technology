package com.vyperion.technology.entities.subtype;

import org.springframework.data.jpa.repository.JpaRepository;

//@RepositoryRestResource(collectionResourceRel = "subtype", path = "subtype")
public interface SubTypeRepository extends JpaRepository<SubType, Integer> {

}
