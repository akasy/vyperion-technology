package com.vyperion.technology.entities.subtype;

import com.vyperion.technology.shared.Master;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/subtype")
public class SubTypeController {

    private final SubTypeService subTypeService;

    public SubTypeController(SubTypeService subTypeService) {
        this.subTypeService = subTypeService;
    }

//    @GetMapping(value = "", produces="application/json")
    @RequestMapping(value = "", method = RequestMethod.GET, produces = Master.media)
    public List<SubType> getAllSubTypes() {
        return subTypeService.getAllSubTypes();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = Master.media)
    public SubType getSubTypeById(@PathVariable int id) {
        return subTypeService.getSubTypeById(id);
    }

}
