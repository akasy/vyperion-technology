package com.vyperion.technology.entities.subtype;

import com.vyperion.technology.entities.technology.Technology;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;


//@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "subtype")
public class SubType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    @NotNull
    private int id;

//    @Embedded
    @Column(name = "name")
    private String name;


    @Column(name = "type")
    private int type;

//    @JoinColumn(name="type")
//    @ManyToOne
//    private Type type;
//
    @OneToMany(mappedBy = "subtype")
    private List<Technology> technologies;


}
