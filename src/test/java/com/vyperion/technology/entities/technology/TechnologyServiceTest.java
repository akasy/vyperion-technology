package com.vyperion.technology.entities.technology;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TechnologyServiceTest {

    @Autowired
    TechnologyService technologyService;

    @MockBean
    TechnologyRepository technologyRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getAllTechnologies() {
        Technology t1 = new Technology("1", "name", "website", "decription", "image", 1);
        Technology t2 = new Technology("2", "name2", "website2", "decription2", "image2", 2);

        when(technologyRepository.findAll()).thenReturn(Arrays.asList(t1, t2));

        assertEquals(technologyService.getAllTechnologies(), technologyRepository.findAll());

        verify(technologyRepository, times(2)).findAll();
        verifyNoMoreInteractions(technologyRepository);
    }

    @Test
    public void getTechnologyById() throws Exception {
        Technology t1 = new Technology("1", "name", "website", "decription", "image", 1);

        when(technologyRepository.findById("1")).thenReturn(Optional.of(t1));

        assertEquals(technologyService.getTechnologyById("1"), technologyRepository
                .findById("1").orElse(new Technology()));

        verify(technologyRepository, times(2)).findById("1");
        verifyNoMoreInteractions(technologyRepository);
    }
}