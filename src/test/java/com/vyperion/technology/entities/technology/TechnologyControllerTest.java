package com.vyperion.technology.entities.technology;


import com.vyperion.technology.entities.subtype.SubTypeService;
import com.vyperion.technology.entities.type.TypeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest
public class TechnologyControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private TechnologyService technologyService;

    @MockBean
    private TypeService typeService;

    @MockBean
    private SubTypeService subTypeService;

    @Before
    public void setUp() throws Exception {
        Mockito.reset(technologyService);
    }

    @Test
    public void getTechnology() throws Exception {
        Technology t1 = new Technology("1", "name", "website",
                "description", "image", 1);
        Technology t2 = new Technology("2", "name2", "website2",
                "description2", "image2", 2);

        when(technologyService.getAllTechnologies()).thenReturn(Arrays.asList(t1, t2));

        mockMvc.perform(get("/technology"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is("1")))
                .andExpect(jsonPath("$[0].name", is("name")))
                .andExpect(jsonPath("$[0].website", is("website")))
                .andExpect(jsonPath("$[0].description", is("description")))
                .andExpect(jsonPath("$[0].image", is("image")))
                .andExpect(jsonPath("$[0].subtype", is(1)))
                .andExpect(jsonPath("$[1].id", is("2")))
                .andExpect(jsonPath("$[1].name", is("name2")))
                .andExpect(jsonPath("$[1].website", is("website2")))
                .andExpect(jsonPath("$[1].description", is("description2")))
                .andExpect(jsonPath("$[1].image", is("image2")))
                .andExpect(jsonPath("$[1].subtype", is(2)));


        verify(technologyService, times(1)).getAllTechnologies();
        verifyNoMoreInteractions(technologyService);
    }

    @Test
    public void getTechnologyById() throws Exception {

        Technology t1 = new Technology("1", "name", "website",
                "description", "image", 1);

        when(technologyService.getTechnologyById("1")).thenReturn(t1);

        mockMvc.perform(get("/technology/1"))
                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.id", is("1")))
                .andExpect(jsonPath("$.name", is("name")))
                .andExpect(jsonPath("$.description", is("description")))
                .andExpect(jsonPath("$.subtype", is(1)));

        verify(technologyService, times(1)).getTechnologyById("1");
        verifyNoMoreInteractions(technologyService);

    }
}















