package com.vyperion.technology.entities.type;

import com.vyperion.technology.entities.subtype.SubType;
import com.vyperion.technology.entities.subtype.SubTypeService;
import com.vyperion.technology.entities.technology.TechnologyService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//https://www.petrikainulainen.net/programming/spring-framework/screencast-unit-testing-of-spring-mvc-controllers-rest-api/
//https://www.petrikainulainen.net/spring-mvc-test-tutorial/
@RunWith(SpringRunner.class)
@WebMvcTest
public class TypeControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private TypeService typeService;

    @MockBean
    private SubTypeService subTypeService;

    @MockBean
    private TechnologyService technologyService;


    @Before
    public void setUp() throws Exception {
        Mockito.reset(typeService);
    }

    @Test
    public void testGetAllTypes() throws Exception{
        List<SubType> empty = new ArrayList<>();
        Type t1 = new Type(1, "name", "description",empty);
        Type t2 = new Type(2, "name2", "description2",empty);


        when(typeService.getAllTypes()).thenReturn(Arrays.asList(t1, t2));

        mockMvc.perform(get("/type"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("name")))
                .andExpect(jsonPath("$[0].description", is("description")))
                .andExpect(jsonPath("$[0].subtype", is(empty)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("name2")))
                .andExpect(jsonPath("$[1].description", is("description2")))
                .andExpect(jsonPath("$[1].subtype", is(empty)));

        verify(typeService, times(1)).getAllTypes();
        verifyNoMoreInteractions(typeService);

//                assertEquals(typeController.getType(), typeService.getType());
    }

    @Test
    public void testGetTypeById() throws Exception {

        List<SubType> empty = new ArrayList<>();
        Type t1 = new Type(1, "name", "description",empty);

        when(typeService.getTypeById(1)).thenReturn(t1);

        mockMvc.perform(get("/type/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("name")))
                .andExpect(jsonPath("$.description", is("description")))
                .andExpect(jsonPath("$.subtype", is(empty)));

        verify(typeService, times(1)).getTypeById(1);
        verifyNoMoreInteractions(typeService);

    }
}