package com.vyperion.technology.entities.type;

import com.vyperion.technology.entities.subtype.SubType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TypeServiceTest {

    @MockBean
    TypeRepository typeRepository;

    @Autowired
    TypeService typeService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getAllTypes() throws Exception {

        List<SubType> empty = new ArrayList<>();

        Type t1 = new Type(1, "name", "description",empty);
        Type t2 = new Type(2, "name2", "description2",empty);


        when(typeRepository.findAll()).thenReturn(Arrays.asList(t1, t2));

        assertEquals(typeService.getAllTypes(), typeRepository.findAll());

        verify(typeRepository, times(2)).findAll();
        verifyNoMoreInteractions(typeRepository);

    }

    @Test
    public void getTypeById() throws Exception {

        List<SubType> empty = new ArrayList<>();

        Type t1 = new Type(1, "name", "description",empty);

        when(typeRepository.findById(Integer.parseInt("1"))).thenReturn(java.util.Optional.of(t1));

        assertEquals(typeService.getTypeById(1), typeRepository
                .findById(1).orElse(new Type()));

        verify(typeRepository, times(2)).findById(1);
        verifyNoMoreInteractions(typeRepository);

    }
}


















