package com.vyperion.technology.entities.subtype;

import com.vyperion.technology.entities.technology.Technology;
import com.vyperion.technology.entities.technology.TechnologyService;
import com.vyperion.technology.entities.type.TypeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest
public class SubTypeControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private SubTypeService subTypeService;

    @MockBean
    private TechnologyService technologyService;

    @MockBean
    private TypeService typeService;


    @Before
    public void setUp() throws Exception {
        Mockito.reset(subTypeService);
    }

    @Test
    public void getAllSubTypes() throws Exception {
        List<Technology> empty = new ArrayList<>();
        SubType t1 = new SubType(1, "name", 1, empty);
        SubType t2 = new SubType(2, "name2", 2, empty);

        when(subTypeService.getAllSubTypes()).thenReturn(Arrays.asList(t1, t2));

        mockMvc.perform(get("/subtype"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("name")))
                .andExpect(jsonPath("$[0].type", is(1)))
                .andExpect(jsonPath("$[0].technologies", is(empty)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("name2")))
                .andExpect(jsonPath("$[1].type", is(2)))
                .andExpect(jsonPath("$[1].technologies", is(empty)));


        verify(subTypeService, times(1)).getAllSubTypes();
        verifyNoMoreInteractions(subTypeService);
    }

    @Test
    public void getSubTypeById() throws Exception {
        List<Technology> empty = new ArrayList<>();
        SubType t1 = new SubType(1, "name", 1, empty);

        when(subTypeService.getSubTypeById(1)).thenReturn(t1);

        mockMvc.perform(get("/subtype/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("name")))
                .andExpect(jsonPath("$.type", is(1)))
                .andExpect(jsonPath("$.technologies", is(empty)));

        verify(subTypeService, times(1)).getSubTypeById(1);
        verifyNoMoreInteractions(subTypeService);

    }
}