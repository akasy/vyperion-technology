package com.vyperion.technology.entities.subtype;

import com.vyperion.technology.entities.technology.Technology;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SubTypeServiceTest {

    @Autowired
    SubTypeService subTypeService;

    @MockBean
    SubTypeRepository subTypeRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getAllSubTypes() {
        List<Technology> empty = new ArrayList<>();

        SubType t1 = new SubType(1, "name", 1, empty);
        SubType t2 = new SubType(2, "name2", 2, empty);

        when(subTypeRepository.findAll()).thenReturn(Arrays.asList(t1, t2));

        assertEquals(subTypeService.getAllSubTypes(), subTypeRepository.findAll());

        verify(subTypeRepository, times(2)).findAll();
        verifyNoMoreInteractions(subTypeRepository);
    }

    @Test
    public void getSubTypeById() {
        List<Technology> empty = new ArrayList<>();
        SubType t1 = new SubType(1, "name", 1, empty);

        when(subTypeRepository.findById(1)).thenReturn(Optional.of(t1));

        assertEquals(subTypeService.getSubTypeById(1), subTypeRepository
                .findById(1).orElse(new SubType()));

        verify(subTypeRepository, times(2)).findById(1);
        verifyNoMoreInteractions(subTypeRepository);
    }
}




















